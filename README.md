# jquery-tss-select

A library used by SchoolRunner for styling select lists.


### Usage

**Include libraries**

	<link href="css/jquery.tss-select.css" rel="stylesheet" type="text/css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="js/vendor/jquery.tss-select.js"></script>

**Create select list**

	<select class="make-tss-select">
		<option value="red">Red</option>
		<option value="green">Green</option>
		<option value="blue">Blue</option>
		<option value="orange">Orange</option>
	</select>
	
**Apply tss-select**

	$(document).ready(function() {
    	$('select.make-tss-select').tssSelect();
	});