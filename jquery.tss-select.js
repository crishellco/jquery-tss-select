/**
 * 
 * @author cmitchell
 */
(function($) {
    $.fn.tssSelect = function(options){
        //set default options
        var defaultOptions = {
            placeholder: 'No Selection'
        };
        
        //merge with passed options
        options = $.extend(defaultOptions, (typeof options === 'object' ? options : {}));
        
        //close on body click
        $('html').click(function() {
            $('.tss-select ul').hide();
        });
        
        return this.each(function(){
            //get input into scoped variable
            var inputElement = $(this);
            
            //check if has already had this plugin applied or is missing multiple attribute
            if (inputElement.siblings('div.tss-select-btn').length !== 0 || inputElement.attr('multiple')) {
                return inputElement;
            }
            
            //wrap input
            inputElement.wrap('<div class="tss-select" />');
            
            //so body click will close only if not within the unordered list
            inputElement.parent().click(function(e){
                e.stopPropagation();
            });

            //hide input
            inputElement.hide();
            
            //add button
            var toggleButton = $('<div class="tss-select-btn" />');
            inputElement.after(toggleButton);
            
            //create options list
            var optionsList = $('<ul />');
            
            //add no value selection
            $('<li data-value="">' + defaultOptions.placeholder + '</li>').appendTo(optionsList);
            
            //populate options list
            inputElement.find('option').each(function() {
                var option = $(this);
                if (option.text().length > 0) {
                    $('<li data-value="' + option.val() + '" class="' + option.data('class') + '">' + option.text() + '</li>').appendTo(optionsList);
                }
            });
            toggleButton.after(optionsList);
            
            //bindings
            inputElement.change(renderSelect);
            
            toggleButton.click(function(e){
                e.preventDefault();
                if (optionsList.is(':visible')) {
                    optionsList.hide();
                } else {
                    //first hide all others
                    $('.tss-select ul').hide();
                    
                    //then show
                    optionsList.show();
                }
            });
            
            optionsList.find('li').click(function(e){
                e.preventDefault();
                
                //hide options list
                optionsList.hide();
                
                //set original select list option and trigger change
                inputElement.val($(this).data('value')).trigger('change');
            });
            
            //functions
            function renderSelect() {
                //get selected option
                var selectedOption = inputElement.find('option:selected'),
                    selectedClass = selectedOption.data('class') ? selectedOption.data('class') : '';
            
                //set button text and class
                var text = selectedOption.text();
                if (text.length === 0) {
                    text = defaultOptions.placeholder;
                }
                toggleButton.text(text).attr('class', 'tss-select-btn ' + selectedClass);
            }
            
            //render for the first time
            renderSelect();
        });
    };
}(jQuery));